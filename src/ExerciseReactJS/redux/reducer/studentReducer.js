import {
  HANDLEONBLUR,
  HANDLEONCHANGE,
  SUBMIT,
  DELETE,
  EDIT,
  UPDATE,
  SEARCH,
} from "../constant/constants";
const initialState = {
  values: {
    id: "",
    name: "",
    number: "",
    email: "",
  },
  errors: {
    id: "",
    name: "",
    number: "",
    email: "",
  },
  listStudent: [
    {
      id: "1",
      name: "Nguyen Van A",
      number: "012345678",
      email: "nguyenvana@gmail.com",
    },
    {
      id: "2",
      name: "Nguyen Van B",
      number: "012345678",
      email: "nguyenvanb@gmail.com",
    },
    {
      id: "3",
      name: "Nguyen Van C",
      number: "012345678",
      email: "nguyenvanc@gmail.com",
    },
    {
      id: "4",
      name: "Nguyen Van D",
      number: "012345678",
      email: "nguyenvand@gmail.com",
    },
  ],
  editList: null,
};
export const StudentReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case HANDLEONCHANGE: {
      const { id, value } = payload.target;
      let error = "";
      // id
      if (id === "id") {
        const reg = /^[0-9]{1,5}$/;
        if (!reg.test(value)) {
          error = `Mã sinh viên phải là số và khoảng từ 1 - 5 chữ số`;
        } else {
          error = ``;
        }
      }
      // name
      if (id === "name") {
        const nameRegex = /^[a-zA-Z ]+$/;
        if (nameRegex.test(value)) {
          error = ``;
        } else {
          error = `Họ tên sinh viên phải là chữ`;
        }
      }
      // number
      if (id === "number") {
        const reg = /^[0-9]{9,11}$/;
        if (!reg.test(value)) {
          error = `Số điện thoại phải là số và khoảng từ 9 - 11 chữ số`;
        } else {
          error = ``;
        }
      }
      if (id === "email") {
        const reg = /^[A-Za-z0-9]+@([\w-]+\.)+[\w-]{2,4}$/;
        if (!reg.test(value)) {
          error = `Email sinh viên phải đúng định dạng`;
        } else {
          error = ``;
        }
      }
      if (value.trim() === "") {
        error = `Trường này không được để trống`;
      }
      console.log(value);
      return {
        ...state,
        values: {
          ...state.values,
          [id]: value,
        },
        errors: {
          ...state.errors,
          [id]: error,
        },
      };
    }
    case HANDLEONBLUR: {
      const { id, value } = payload.target;
      let error = "";
      // id
      if (id === "id") {
        const reg = /^[0-9]{1,5}$/;
        if (!reg.test(value)) {
          error = `Mã sinh viên phải là số và khoảng từ 1 - 5 chữ số`;
        } else {
          error = ``;
        }
      }
      // name
      if (id === "name") {
        const nameRegex = /^[a-zA-Z ]+$/;
        if (nameRegex.test(value)) {
          error = ``;
        } else {
          error = `Họ tên sinh viên phải là chữ`;
        }
      }
      // number
      if (id === "number") {
        const reg = /^[0-9]{9,11}$/;
        if (!reg.test(value)) {
          error = `Số điện thoại phải là số và khoảng từ 9 - 11 chữ số`;
        } else {
          error = ``;
        }
      }
      if (id === "email") {
        const reg = /^[A-Za-z0-9]+@([\w-]+\.)+[\w-]{2,4}$/;
        if (!reg.test(value)) {
          error = `Email sinh viên phải đúng định dạng`;
        } else {
          error = ``;
        }
      }
      if (value.trim() === "") {
        error = `Trường này không được để trống`;
      }
      return {
        ...state,
        values: {
          ...state.values,
          [id]: value,
        },
        errors: {
          ...state.errors,
          [id]: error,
        },
      };
    }
    case SUBMIT: {
      payload.preventDefault();
      const { values, errors } = state;
      let isValid = true;
      for (const key in values) {
        if (values[key] === "" || errors[key] !== "") {
          isValid = false;
        }
      }
      const index = state.listStudent.findIndex((item) => {
        if (item.id === state.values.id) {
          return true;
        }
      });
      if (index !== -1) {
        console.log("Mã sinh viên bạn nhập bị trùng");
        isValid = false;
      }
      if (!isValid) {
        console.log("Vui lòng nhập tất cả thông tin");
        return { ...state };
      }
      console.log("Thêm thành công");
      const newStudent = [...state.listStudent, state.values];
      state.listStudent = newStudent;
      return { ...state };
    }
    case DELETE: {
      const index = state.listStudent.findIndex((item) => {
        return item.id === payload;
      });
      if (index !== -1) {
        const newStudentList = [...state.listStudent];
        newStudentList.splice(index, 1);
        return { ...state, listStudent: newStudentList };
      }
    }
    case EDIT: {
      const index = state.listStudent.find((item) => {
        return item.id === payload;
      });
      return { ...state, editList: index, values: index };
    }
    case UPDATE: {
      const index = state.listStudent.findIndex((item) => {
        return item.id === state.editList.id;
      });
      const newEditList = [...state.listStudent];
      newEditList[index] = state.values;
      return {
        ...state,
        listStudent: newEditList,
        values: {
          id: "",
          name: "",
          number: "",
          email: "",
        },
        editList: null,
      };
    }
    case SEARCH: {
      console.log(payload);
      // const query = document.getElementById("valueSearch").value;
      // console.log(query);
      var searchValue = [...state.listStudent];
      searchValue = searchValue.filter((item) => {
        return item.name.toLowerCase() === payload.toLowerCase();
      });
      console.log(searchValue);
      state.listStudent = searchValue;
      return { ...state };
    }
    default:
      return state;
  }
};
