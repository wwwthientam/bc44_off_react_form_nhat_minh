import React, { Component } from "react";
import List from "./List";
import { connect } from "react-redux";
import {
  viewOnBlur,
  viewOnChange,
  viewSubmit,
  viewUpdate,
} from "./redux/action/studentAction";
class Form extends Component {
  render() {
    let { value, error, edit } = this.props;
    let { id, name, number, email } = value;

    return (
      <div>
        <h1 className="text-center bg-dark text-white p-4">
          Exercise React JS Form
        </h1>
        <div className="container pt-4">
          <h1 className="bg-dark text-white pl-5">Thông tin sinh viên</h1>
          <form
            onSubmit={(event) => {
              this.props.handleSubmit(event);
            }}
          >
            <div className="row">
              <div className="col-6 pb-4 pt-2">
                <label>Mã Sinh Viên:</label>
                <input
                  type="text"
                  id="id"
                  name="id"
                  value={id}
                  className="form-control"
                  placeholder="ID: 12345"
                  disabled={edit}
                  onChange={(event) => {
                    this.props.handleOnChange(event);
                  }}
                  onBlur={(event) => {
                    this.props.handleOnBlur(event);
                  }}
                />
                {error.id !== "" ? (
                  <span className="text-danger">{error.id}</span>
                ) : (
                  ""
                )}
              </div>
              <div className="col-6 pb-4 pt-2">
                <label>Họ tên Sinh Viên:</label>
                <input
                  type="text"
                  id="name"
                  name="name"
                  value={name}
                  className="form-control"
                  placeholder="Your Name"
                  onChange={(event) => {
                    this.props.handleOnChange(event);
                  }}
                  onBlur={(event) => {
                    this.props.handleOnBlur(event);
                  }}
                />
                {error.name && (
                  <span className="text-danger">{error.name}</span>
                )}
              </div>
              <div className="col-6">
                <label>Số điện thoại:</label>
                <input
                  type="text"
                  id="number"
                  name="number"
                  value={number}
                  className="form-control"
                  placeholder="Your Phone Number"
                  onChange={(event) => {
                    this.props.handleOnChange(event);
                  }}
                  onBlur={(event) => {
                    this.props.handleOnBlur(event);
                  }}
                />
                {error.number && (
                  <span className="text-danger">{error.number}</span>
                )}
              </div>
              <div className="col-6">
                <label>Email:</label>
                <input
                  type="text"
                  id="email"
                  name="email"
                  value={email}
                  className="form-control"
                  placeholder="your.email@gmail.com"
                  onChange={(event) => {
                    this.props.handleOnChange(event);
                  }}
                  onBlur={(event) => {
                    this.props.handleOnBlur(event);
                  }}
                />
                {error.email && (
                  <span className="text-danger">{error.email}</span>
                )}
              </div>
            </div>
            {edit ? (
              <button
                type="button"
                className="btn btn-primary mt-4 mb-4"
                onClick={(event) => {
                  this.props.handleUpdate(event);
                }}
              >
                Cập nhật
              </button>
            ) : (
              <button className="btn btn-danger mt-4 mb-4">
                Thêm sinh viên
              </button>
            )}
          </form>
        </div>
        <List />
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    value: state.StudentReducer.values,
    error: state.StudentReducer.errors,
    edit: state.StudentReducer.editList,
  };
};
let mapDispatchToState = (dispatch) => {
  return {
    handleOnChange: (event) => {
      dispatch(viewOnChange(event));
    },
    handleOnBlur: (event) => {
      dispatch(viewOnBlur(event));
    },
    handleSubmit: (event) => {
      dispatch(viewSubmit(event));
    },
    handleUpdate: () => {
      dispatch(viewUpdate());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToState)(Form);
